tool

extends HBoxContainer


export var image: Texture
export var image2: Texture
export var text: String

# Called when the node enters the scene tree for the first time.
func _ready():
	$AspectRatioContainer/Icon.texture = image
	$Text.text = text
	$AspectRatioContainer2/Icon.texture = image2
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
