extends Spatial

var air_vector = Vector3.RIGHT
var next_air_vector = Vector3.RIGHT
var time = 0
var rng = RandomNumberGenerator.new()
var next_air_vector_update = 20
var next_second = false
var has_started = false

func _ready():
	return

func _process(delta):
	if !has_started:
		if Input.is_action_just_pressed("reroll_wind"):
			has_started = true
		return
	
	time += delta
	if int(time) > int(time - delta):
		next_second = true
	else:
		next_second = false
	 
	if int(time) % next_air_vector_update == 0 && next_second:
		next_air_vector_update = rng.randi_range(20, 90)
		next_air_vector = update_air_vector(delta)
	elif Input.is_action_just_pressed("reroll_wind"):
		next_air_vector = update_air_vector(delta)
	
	if next_air_vector != air_vector:
		air_vector = lerp(air_vector, next_air_vector, delta / 5)
	
	$RayCast.set_cast_to(air_vector * 20)
	
	return

func update_air_vector(delta) -> Vector3:
	var x_amount = rng.randf_range(-delta * 6, delta * 6)
	var z_amount = rng.randf_range(-delta * 6, delta * 6)
	
	return Vector3(x_amount, 0, z_amount).normalized()

func get_air_vector() -> Vector3:
	return air_vector.normalized()
