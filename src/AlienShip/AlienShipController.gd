extends Spatial

var absorbing_particles_count = 4
var current_destination_index = -1
var current_destination = null

onready var old_destination = self.global_transform.origin
onready var destinations = get_node("/root/Main/ShipDestinations")
onready var ship_alert_sound = preload("res://assets/AlienShip/sounds/ship_alert.wav")

signal wake_up()

func _process(delta):
	if !$RotateTimer.is_stopped():
		rotate_to_position(delta)
	
	if !$TranslateTimer.is_stopped():
		translate_to_position()
	
	return


func _on_DetectRange_body_entered(body):
	if !$DelayTimer.is_stopped() || !$RotateTimer.is_stopped() || !$TranslateTimer.is_stopped():
		
		return
	
	if body.is_in_group("Boat"):
		print("entered alien ship space")
		if current_destination_index == 5 && get_node("/root/Main/OceanWorld") != null:
			print("we are in ocean world")
			return # boat must be in CloudWorld to activate
		
		if current_destination_index == 6:
			absorbing_particles_count = 8 # cloudworld gets too much particles
		
		current_destination_index += 1
		emit_signal("wake_up")
		
		if destinations.get_child_count() <= current_destination_index:
			return
		
		current_destination = destinations.get_children()[current_destination_index]
		old_destination = self.global_transform.origin
		travel_to_new_location()
	
	return

func travel_to_new_location():
	$DelayTimer.start()
	$EmissionStopTimer.start()
	
	return

func rotate_to_position(delta):
	self.rotation = lerp(self.rotation, current_destination.rotation, delta / 5)
	#self.global_transform.basis.slerp(self.global_transform.looking_at(Vector3(current_destination.global_transform.origin.x, self.global_transform.origin.y, current_destination.global_transform.origin.z), Vector3.UP).basis, delta / $RotateTimer.wait_time)
	
	return

func translate_to_position():
	var percent = ($TranslateTimer.wait_time - $TranslateTimer.time_left) / $TranslateTimer.wait_time
	self.global_transform.origin = lerp(old_destination, current_destination.global_transform.origin, percent)
	
	return

func _on_TranslateTimer_timeout():
	old_destination = self.global_transform.origin
	
	absorbing_particles_count *= 4
	$AbsorbingParticles.amount = absorbing_particles_count
	$AbsorbingParticles.emitting = true
	
	if absorbing_particles_count > 63:
		$Armature/AlienShipShield.visible = true
	
	for body in $DetectRange.get_overlapping_bodies():
		if body.is_in_group("Boat"):
			$DetectRange.emit_signal("body_entered", body)
	
	return

func _on_DelayTimer_timeout():
	$RotateTimer.start()
	$TranslateTimer.start()
	
	return

func _on_EmissionStopTimer_timeout():
	$AbsorbingParticles.emitting = false
	
	return

func _on_AlienShip_wake_up():
	$AlienSounds.play()
	
	return

func _on_AlienSounds_finished():
	$AlienSounds.stop()
	$AlienSounds.set_stream(ship_alert_sound)
	
	return
