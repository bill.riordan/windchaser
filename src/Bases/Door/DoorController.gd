extends Spatial

signal traversed()

func _ready():
	return

func _on_TraverseDetection_body_entered(body):
	if body.is_in_group("Boat"):
		emit_signal("traversed") # tell the world we traversed a portal
	
	return

# used for easily testing traversals
func _on_Timer_timeout():
	# emit_signal("traversed")
	return
