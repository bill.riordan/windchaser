extends KinematicBody


const SPEED: int = 45
const SOUND_DB: int = 25

var is_ocean = true
var strength = 0

onready var prev_loc: Vector3 = Vector3()
onready var ocean = get_node("/root/Main/Ocean")
onready var air = get_node("/root/Main/Air")
onready var camera = get_node("/root/Main/Position3D")

signal location_changed(location)

func _ready():
	self.add_to_group("Boat")
	
	return

func _process(_delta):
	get_wave_action()
	rotate_mast()
	rotate_flags()
	update_sounds_strength()
	calculate_input()
	
	var forward_strength = get_air_velocity_speed_contribution()
	if forward_strength:
		move_and_slide(Vector3.FORWARD.rotated(Vector3.UP, self.rotation.y) * forward_strength * SPEED, Vector3.UP)
	
	if prev_loc != self.global_transform.origin:
		prev_loc = self.global_transform.origin
		emit_signal("location_changed", self.global_transform.origin)

	return

func get_wave_action():
	self.global_transform.origin.y = ocean.get_wave_height_at_position(Vector2(self.global_transform.origin.x, self.global_transform.origin.z))
	var bow_height = ocean.get_wave_height_at_position(Vector2($BowPosition.global_transform.origin.x, $BowPosition.global_transform.origin.z))
	self.look_at(Vector3($BowPosition.global_transform.origin.x, bow_height, $BowPosition.global_transform.origin.z), Vector3.UP)

	return

func calculate_input():
	var direction = Vector3()
	direction.z = Input.get_action_strength("back") - Input.get_action_strength("forward")
	var lateral_strength = Input.get_action_strength("left") - Input.get_action_strength("right")
	
	if lateral_strength:
		self.rotate_y(lateral_strength / 200)
	
	return direction.rotated(Vector3.UP, self.rotation.y)

func get_air_velocity_speed_contribution():
	var air_vector: Vector3 = Basis(air.get_air_vector()).get_euler().normalized()
	var air_magnitude = sqrt( (air_vector.x * air_vector.x) + (air_vector.z * air_vector.z) )
	var mast_vector: Vector3 = $BigBoat/CenterMast.global_transform.origin.direction_to($BigBoat/CenterMast/FacingDirection.global_transform.origin)

	var sky_multiplier = 1
	if !is_ocean:
		sky_multiplier = 2
	
	strength = air_vector.dot(mast_vector) * air_magnitude * sky_multiplier
	if strength <= .1:
		return 0.1 * sky_multiplier
	
	return strength

func rotate_flags():
	for flag in $BigBoat/Flags.get_children():
		var air_pos = flag.global_transform.origin + air.get_air_vector()
		var original_pos = flag.global_transform.origin
		flag.look_at(air_pos, Vector3.UP)
		flag.global_transform.origin = original_pos
	
	return

func rotate_mast():
	var rotate_mast_amount = Input.get_action_strength("rotate_mast_left") - Input.get_action_strength("rotate_mast_right")
	
	if (rotate_mast_amount > 0 && !$BigBoat/CenterMast.rotation_degrees.y > 70) || (rotate_mast_amount < 0 && !$BigBoat/CenterMast.rotation_degrees.y < -70):
		$BigBoat/CenterMast.rotate_object_local(Vector3.UP, rotate_mast_amount / 170)
		$BigBoat/FrontMast.rotate_object_local(Vector3.UP, rotate_mast_amount / 170)
		$BigBoat/BackMast.rotate_object_local(Vector3.UP, rotate_mast_amount / 170)
	
	return

func update_sounds_strength():
	$FastBoatSounds.unit_db = strength * self.SOUND_DB
	$SlowBoatSounds.unit_db = (1 - strength) * self.SOUND_DB
	if is_ocean:
		get_node("/root/Main/Position3D/Camera/WindSounds").unit_db = strength * self.SOUND_DB / 5
	
	return

func _on_Main_traverse_to_clouds():
	is_ocean = false
	get_node("/root/Main/Position3D/Camera/WindSounds").unit_db = self.SOUND_DB / 5
	#stop ocean sounds
	$SlowBoatSounds.playing = false
	$FastBoatSounds.playing = false
	
	return

func _on_BoatSounds_finished():
	$BoatSounds.play()
	
	return

func _on_SlowBoatSounds_finished():
	$SlowBoatSounds.play()
	
	return

func _on_FastBoatSounds_finished():
	$FastBoatSounds.play()
	
	return
