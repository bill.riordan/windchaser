extends Spatial


func _ready():
	return

func _on_CloudWorld_boat_location_changed(location):
	self.global_transform.origin.x = location.x
	self.global_transform.origin.z = location.z
	return
