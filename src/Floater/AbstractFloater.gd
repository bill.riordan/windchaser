extends KinematicBody

onready var ocean = get_node("/root/Main/OceanWorld/Ocean")

func _process(_delta):
	get_wave_height_at_position(self.global_transform.origin)
	return

func get_wave_height_at_position(position: Vector3) -> void:
	self.global_transform.origin.y = ocean.get_wave_height_at_position(Vector2(position.x, position.z))
	return
