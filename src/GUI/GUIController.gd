extends ViewportContainer

var controls_visible = true

func _ready():
	return

func _process(_delta):
	# update_screen_size()
	if Input.is_action_just_pressed("settings"):
		toggle_controls()
	return

func update_screen_size():
	if !Engine.editor_hint:
		self.rect_size = get_viewport().size
		$Viewport.size = get_viewport().size
	pass

func toggle_controls():
	controls_visible = !controls_visible
	$Viewport/HUD/GridContainer/Left/GridContainer.visible = controls_visible
	$HideTimer.start()
	return

func _on_HideTimer_timeout():
	$Viewport/HUD/GridContainer/Left/GridContainer.visible = false
	controls_visible = false
	pass
