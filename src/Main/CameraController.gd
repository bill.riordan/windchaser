extends Position3D

var zoom_amounts = [30,45,110]
var current_zoom = 0

var change_camera = false

onready var zoom_positions = get_node("../ZoomPositions")

func _ready():
	return


func _process(delta):
	pan()
	toggle_zoom()
	if change_camera:
		change_to_new_camera(delta)
	return

func pan():
	var horizontal = Input.get_action_strength("pan_left") - Input.get_action_strength("pan_right")
	var vertical = Input.get_action_strength("pan_up") - Input.get_action_strength("pan_down")
	self.rotate_y(horizontal / 175)
	if (vertical > 0 && !self.rotation_degrees.x > 38) || (vertical < 0 && !self.rotation_degrees.x < -40):
		self.rotate_object_local(Vector3.RIGHT, vertical / 175)
	
	return

func toggle_zoom():
	if Input.is_action_just_pressed("toggle_zoom"):
		current_zoom = (current_zoom + 1) % 3
		change_camera = true
		
	return

func change_to_new_camera(delta):
	var fov = zoom_amounts[current_zoom]
	if $Camera.fov != fov:
		$Camera.global_transform.origin = lerp($Camera.global_transform.origin, $ZoomPositions.get_child(current_zoom).global_transform.origin, delta * 2)
		$Camera.fov = lerp($Camera.fov, fov, delta * 2)
	else:
		change_camera = false
	return

func _on_Boat_location_changed(location):
	self.global_transform.origin.x = location.x
	self.global_transform.origin.y = location.y
	self.global_transform.origin.z = location.z
	
	return
