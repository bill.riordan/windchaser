extends WorldEnvironment


var ocean_environment = load("res://assets/Main/Main.tres")
var cloud_environment = load("res://assets/Main/Cloud.tres")
var current_environment = ocean_environment
var is_ocean = true

onready var cloud_world = preload("res://src/CloudWorld/CloudWorld.tscn")
onready var ocean_world = preload("res://src/oceanWorld/OceanWorld.tscn")

signal traverse_to_clouds()

func _ready():
	self.environment = current_environment
	
	return


func traversal_to_clouds():
	current_environment = cloud_environment
	self.environment = current_environment
	$OceanWorld.queue_free()
	self.add_child(cloud_world.instance())
	
	var new_world = self.get_node("CloudWorld")
	$Boat.connect("location_changed", new_world, "_on_Boat_location_changed")
	
	emit_signal("traverse_to_clouds")
	
	return

func traversal_to_ocean():
	current_environment = ocean_environment
	self.environment = current_environment
	$CloudWorld.queue_free()
	self.add_child(ocean_world.instance())
	
	var new_world = self.get_node("OceanWorld")
	$Boat.connect("location_changed", new_world, "_on_Boat_location_changed")
	
	return

func _on_Door_traversed():
	print("traversing...")
	if is_ocean:
		traversal_to_clouds()
		is_ocean = false
	
	return
