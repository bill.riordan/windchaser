extends Spatial

var material

var time: float = 0.0
var offset: Vector2 = Vector2()
var center_vertex = null
var height: float = 0.0
var noise = preload("res://assets/Ocean/ocean_depth.tres").noise

func _ready():
	material = $Mesh.get_material()
	pass


func _process(delta):
	self.time += delta
	material.set_shader_param("height", abs(noise.get_noise_2d(self.global_transform.origin.x, self.global_transform.origin.z) * 50))
	material.set_shader_param("elapsed", self.time)
	pass

func get_wave_height_at_position(location: Vector2):
	var wave_speed = material.get_shader_param("wave_speed")
	var wave_size = material.get_shader_param("wave_size")
	var wave_height = material.get_shader_param("height")
	var tile = material.get_shader_param("tile")
	
	var offset_time = self.time * wave_speed
	var uv = Vector2(location / tile) * wave_size
	var d1 = fmod(uv.x + uv.y, PI * 2)
	var d2 = fmod((uv.x + uv.y + 0.25) * 1.3, PI * 6)
	d1 = offset_time * 0.07 + d1
	d2 = offset_time * 0.5 + d2
	var dist = Vector2(
		sin(d1) * 0.15 + sin(d2) * 0.05,
		cos(d1) * 0.15 + cos(d2) * 0.05
	)
	var result = dist.y * wave_height;
	
	return result

func _on_Boat_location_changed(location):
	offset = Vector2(location.x, location.z)
	self.global_transform.origin.x = location.x
	self.global_transform.origin.z = location.z
	material.set_shader_param("offset",Vector2(location.x, location.z))
	
	return
