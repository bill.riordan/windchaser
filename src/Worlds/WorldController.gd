extends Spatial

signal boat_location_changed(location)

func _on_Boat_location_changed(location):
	emit_signal("boat_location_changed", location)
	self.get_child(0).global_transform.origin.x = location.x
	self.get_child(0).global_transform.origin.z = location.z
	pass

